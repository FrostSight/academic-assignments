<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Storage </title>
    <link rel="stylesheet" href="storagestyle.css">
</head>
<body>
    <?php

        $myserver = "localhost";
        $username = "root";
        $password = "";
        $databasename = "mydb";

        $connection = mysqli_connect($myserver, $username, $password, $databasename);

        // Creating database
        // $sql_create = "create database mydb";
        // mysqli_query($connection, $sql_create);

        // Creating Table
        // $create_table = "create table marks(
        //                     name varchar(100),
        //                     att varchar(100),
        //                     quiz varchar(100),
        //                     final varchar(100)
        //                 )";
        // mysqli_query($connection, $create_table);

        // Drop Table
        // $drop_table = "drop table marks";
        // mysqli_query($connection, $drop_table);

        $insert_values = "insert into marks(name, att, quiz, final)
                        values('$_POST[name]', '$_POST[attendance_mark]', '$_POST[quiz_mark]', '$_POST[final_mark]')";
        mysqli_query($connection, $insert_values);
        
        $result_query = "SELECT * FROM marks";
        $result = mysqli_query($connection, $result_query);

        echo "<table>"; 
		echo "<tr>"; 
		echo "<th>Name</th><th>Attendance Mark</th><th>Quiz Mark</th><th>Final Mark</th>"; 
		echo "</tr>";
        
        if (mysqli_num_rows($result) > 0) {
            while($row = mysqli_fetch_assoc($result)) {
                echo "<tr>"; 
				echo "<td>" . $row['name'] . "</td>"; 
				echo "<td>" . $row['att'] . "</td>";
                echo "<td>" . $row['quiz'] . "</td>";  
                echo "<td>" . $row['final'] . "</td>";
				echo "</tr>";
            }
        }
    ?>
</body>
</html>