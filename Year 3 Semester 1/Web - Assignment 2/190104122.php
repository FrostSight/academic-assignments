<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Second Assignment</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <h1>Prime Number from 2 to 30</h1>
    <div>
    <?php 
        echo "<table border=1>";
        echo "<tr><th>Prime Number</th>";

        $x = 2;
        $y = 30;

        for($i = $x; $i <= $y; $i++) {
            $n = 1;
            for($j = 2; $j < $i / 2; $j++) {

                if($i % $j == 0){
                    $n = 0;
                    break;
                }
            }
            if ($n == 1){
                echo "<tr><td>";
                echo $i." ";
            } 
        }

        echo "</table>";
    ?>

    </div>
</body>
</html>