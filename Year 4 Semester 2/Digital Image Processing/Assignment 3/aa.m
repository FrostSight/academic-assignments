img = rgb2gray(imread('tj.jpg'));

prompt='Enter value of sigma: ';
sg = input(prompt);

sum_k = 0;
size_of_kernel = 7; 
KN = ones(size_of_kernel, size_of_kernel);

for i = 1:size_of_kernel
    for j = 1:size_of_kernel
        dist = (i-ceil(size_of_kernel/2))^2 + (j-ceil(size_of_kernel/2))^2;
        KN(i,j) = exp((-1*dist) / (2*sg*sg));
        sum_k = sum_k + KN(i,j);
    end
end
KN = KN/sum_k;

[r, c] = size(img);
outputImage = zeros(r, c);

padding = padarray(img, [floor(size_of_kernel/2), floor(size_of_kernel/2)]);

for i = 1:r
    for j = 1:c
        temp = double(padding(i:i+size_of_kernel-1, j:j+size_of_kernel-1));
        filt = temp .* KN;
        outputImage(i, j) = sum(filt(:));
    end
end

outputImage = uint8(outputImage);
imwrite(outputImage, 'output.jpg');

figure;
subplot(1,2,1);
imshow(img);
title('Original');
subplot(1,2,2);
imshow(outputImage);
title('Output Image');