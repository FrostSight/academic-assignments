% (a)
imgfirst = imread("t.jpg");
img = rgb2gray(imgfirst);
[rows, columns] = size(img);

% (b)
columnSum = zeros(1, columns);
for i = 1:rows
    for j = 1:columns
        columnSum(img(i, j)) = columnSum(img(i, j)) + 1;
    end
end

% (c)
PDF = columnSum / sum(columnSum);

% (d)
imwrite(img, 'newt.jpg');
imfinfo('newt.jpg');

k = 8;
L = 2 ^ k;
CDF = zeros(1, L);
CDF(1) = PDF (1);

for i = 2:L
    CDF(i) = PDF(i) + CDF(i - 1);
end

% (e)
CDF = CDF * (L -1);

% (f)
CDF = round(CDF);

% (h)
newImg = img;
for i = 1 : rows
    for j = 1 : columns
        newImg(i, j) = CDF(img(i, j) + 1);
    end
end

histoGram = zeros(1, L);
for i = 1 : rows
    for j = 1 : columns
        histoGram(newImg(i, j) + 1) = histoGram(newImg(i, j) + 1) + 1;
    end
end

% (g)
figure
subplot(2, 2, 1);
imshow(imgfirst);
title('Original Image');
subplot(2, 2, 2);
imshow(newImg);
title('Modified Image');

% (plots of h)
subplot(2, 2, 3);
bar(columnSum);
title('Original Image Histogram');
subplot(2, 2, 4);
bar(histoGram);
title('Modified Image Histogram');

%%%%%%Specification
imgsecond = imread("pep.jpg");
img2 = rgb2gray(imgsecond);
[row, col] = size(img2);

columnSum2 = zeros(1, col);
for i = 1:row
    for j = 1:col
        columnSum2(img2(i, j)) = columnSum2(img2(i, j)) + 1;
    end
end
PDF2 = columnSum2 / sum(columnSum2);

imwrite(img, 'newt2.jpg');
imfinfo('newt2.jpg');

k2 = 8;
L2 = 2 ^ k;
CDF2 = zeros(1, L2);
CDF2(1) = PDF2(1);
for i = 2:L2
    CDF2(i) = PDF2(i) + CDF2(i - 1);
end

CDF2 = CDF2 * (L2 -1);

CDF2 = round(CDF2);

newImg2 = img2;
for i = 1 : row
    for j = 1 : col
        newImg2(i, j) = CDF2(img2(i, j) + 1);
    end
end

histoGram2 = zeros(1, L2);
for i = 1 : row
    for j = 1 : col
        histoGram2(newImg2(i, j) + 1) = histoGram2(newImg2(i, j) + 1) + 1;
    end
end

figure
subplot(2, 2, 1);
imshow(imgsecond);
title('Original Image');
subplot(2, 2, 2);
imshow(newImg2);
title('New Image');

subplot(2, 2, 3);
bar(columnSum2);
title('Original Image Histogram');
subplot(2, 2, 4);
bar(histoGram2);
title('New Image Histogram');

B=imhistmatch(img,img2);
figure
imshow(B);
imwrite(B, 'output2.jpg');
