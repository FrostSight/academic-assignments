% (a)
I = imread('cameraman.png');

% (b)
A = double(min(I(:)))

% (c)
B = double(max(I(:)))

% (d)
D = B - A;
M = (2 ^ (floor(log2(double(B))) + 1)) - 1;

[rows, columns] = size(I);
I = double(I);
R = double(I);
% (e)
for i = 1 : rows
    for j = 1: columns
        R(i, j) = (((I(i, j) - A) / D) * M) + A;
    end
end
R = uint8(R);
I = uint8(I);

% (f)
figure
subplot(2, 2, 1);
imshow(I);
title('Original Image');
subplot(2, 2, 2);
imshow(R);
title('New Image');


% (g)
histoGram1 = zeros(1, M + 1);
for i = 1 : rows
    for j = 1 : columns
        histoGram1(I(i, j) + 1) = histoGram1(I(i, j) + 1) + 1;
    end
end

histoGram2 = zeros(1, M + 1);
for i = 1 : rows
    for j = 1 : columns
        histoGram2(R(i, j) + 1) = histoGram2(R(i, j) + 1) + 1;
    end
end

subplot(2, 2, 3);
bar(histoGram1);
title('Original Image Histogram');
subplot(2, 2, 4);
bar(histoGram2);
title('New Image Histogram');