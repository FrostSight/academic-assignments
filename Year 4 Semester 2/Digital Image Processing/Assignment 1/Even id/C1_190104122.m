img1 = imread('input1.jpg');
img2 = imread('input2.jpg');

img1 = rgb2gray(img1);
img2 = rgb2gray(img2);

output = uint8(zeros(600, 600));

for i = 1: 600
    for j = 1: 600
        if mod(floor(j/100), 2) == 0
            output(i, j) = img2(i, j);
        else
            output(i, j) = img1(i, j);
        end
    end
end

figure;
imshow(output);
imwrite(output, 'output.jpg');