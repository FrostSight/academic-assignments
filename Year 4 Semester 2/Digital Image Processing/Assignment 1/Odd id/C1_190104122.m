input_img=imread('input1.jpg');
input_img=imresize(input_img,[400,400]);
input_img = rgb2gray(input_img);

%imtool(input_img);
figure; 
imshow(input_img);

[row, col] = size(input_img);
mirror_image = uint8(ones(row, col));

for i = 1:row
    for j = 1:col
        new = col-j+1;
        mirror_image(i,j) = input_img(i,new);
    end
end

figure;
imshow(mirror_image);

new_row = 400;
new_col = 800;
output_image = zeros(new_row, new_col, 'uint8');
output_image(1:row, 1:col) = input_img;
output_image(1:row, col + 1:end) = mirror_image;
figure; 
imshow(output_image);
%imtool(output_image);

imwrite(output_image, 'out.jpg');