I= imread('flower.png');
I=rgb2gray(I);
[row,col]=size(I);
for i = row/4 : (3*row)/4
    for j = col/4 : col
        if(I(i,j) >= 80)
            I(i,j)= (I(i,j)*0.6)+I(i,j);
        else
            I(i,j)= I(i,j)-I(i,j)*0.7;
        end
    end
end
figure;
subplot(2,2,1);
imshow(I);
H=ones(1,256);
for i = 1:row
    for j=1:col
        H(I(i,j)+1) = H(I(i,j)+1) +1;
    end
end
subplot(2,2,2);
bar(H);
