I = imread('flower.png');

I=rgb2gray(I);
figure;
imshow(I);
[row,col]=size(I);
for i =row/4 : (3*row)/4
    for j= 1:(col*3)/4
        if(I(i,j)>=70)
            I(i,j)=(I(i,j)*0.6)+I(i,j);
        else
            I(i,j)=I(i,j)-I(i,j)*0.7;
        end
    end
end

figure;
imshow(I);
H=ones(1,256);
for i=1:row
    for j=1:col
        H(I(i,j)+1)=H(I(i,j)+1)+1;
    end
end
figure;
bar(H);
    