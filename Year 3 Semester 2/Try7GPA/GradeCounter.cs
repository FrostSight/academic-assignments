﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Try7GPA
{
    internal class GradeCounter
    {
        public static double mark, gpa;

        public static double grading (double mark, double credit)
        {
            if (mark < 40)
            {
                gpa = 0.00 * credit;
            }
            else if (mark < 45)
            {
                gpa = 2.00 * credit;
            }
            else if (mark < 50)
            {
                gpa = 2.25 * credit;
            }
            else if (mark < 55)
            {
                gpa = 2.50 * credit;
            }
            else if (mark < 60)
            {
                gpa = 2.75 * credit;
            }
            else if (mark < 65)
            {
                gpa = 3.00 * credit;
            }
            else if (mark < 70)
            {
                gpa = 3.25 * credit;
            }
            else if (mark < 75)
            {
                gpa = 3.50 * credit;
            }
            else if (mark < 80)
            {
                gpa = 3.75 * credit;
            }
            else if (mark < 100)
            {
                gpa = 4.00 * credit;
            }
            return gpa;
        }
    }
}
