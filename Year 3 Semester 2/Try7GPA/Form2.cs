﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Try7GPA
{
    public partial class Form2 : Form
    {

        public string name { get; set; }
        public double gpa { get; set; }
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            NameLabel.Text = name;
            Console.WriteLine(gpa.ToString());
            GPA_Label.Text = gpa.ToString();
        }

        private void backbutton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }
    }
}
