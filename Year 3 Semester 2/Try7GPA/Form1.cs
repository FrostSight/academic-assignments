﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Try7GPA
{
    public partial class Form1 : Form
    {
        double cse3200GradePoint, cse3211GradePoint, cse3213GradePoint, cse3214GradePoint, cse3215GradePoint, cse3216GradePoint, cse3223GradePoint, cse3224GradePoint, cse3207GradePoint;

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((!char.IsLetter(e.KeyChar)) && (!char.IsWhiteSpace(e.KeyChar)) && (!char.IsControl(e.KeyChar)))
            {
                e.Handled = true;
            }
        }

        public double allGpa;
        public string strName = "";

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "" || comboBox1.Text == "")
            {
                MessageBox.Show(
                    "Empty field",
                    "Invalid input",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error
                    );
            }
            else
            {
                double cse3200 = Convert.ToDouble(numericUpDown1.Text);
                double cse3211 = Convert.ToDouble(numericUpDown2.Text);
                double cse3213 = Convert.ToDouble(numericUpDown3.Text);
                double cse3214 = Convert.ToDouble(numericUpDown4.Text);
                double cse3215 = Convert.ToDouble(numericUpDown5.Text);
                double cse3216 = Convert.ToDouble(numericUpDown6.Text);
                double cse3223 = Convert.ToDouble(numericUpDown7.Text);
                double cse3224 = Convert.ToDouble(numericUpDown8.Text);
                double cse3207 = Convert.ToDouble(numericUpDown9.Text);

                cse3200GradePoint = GradeCounter.grading(cse3200, 0.75);
                cse3211GradePoint = GradeCounter.grading(cse3211, 3.00);
                cse3213GradePoint = GradeCounter.grading(cse3213, 3.00);
                cse3214GradePoint = GradeCounter.grading(cse3214, 1.50);
                cse3215GradePoint = GradeCounter.grading(cse3215, 3.00);
                cse3216GradePoint = GradeCounter.grading(cse3216, 0.75);
                cse3223GradePoint = GradeCounter.grading(cse3223, 3.00);
                cse3224GradePoint = GradeCounter.grading(cse3224, 0.75);
                cse3207GradePoint = GradeCounter.grading(cse3207, 3.00);


                allGpa = (cse3200GradePoint + cse3211GradePoint + cse3213GradePoint + cse3214GradePoint + cse3215GradePoint + cse3216GradePoint + cse3223GradePoint + cse3224GradePoint + cse3207GradePoint) / 18.75;
                Console.WriteLine(allGpa);
                strName = textBox1.Text;
                Console.WriteLine(strName);

                this.Hide();
                Form2 fm2 = new Form2();
                fm2.name = strName;
                fm2.gpa = allGpa;
                fm2.ShowDialog();
                this.Show();
            }
        }
    }
}
