#include <bits/stdc++.h>
using namespace std;

int arr[10];
int brr[10][10];

int mcmultiplication(int i, int j)
{
    if (i == j)
        return 0;

    for (int k = i; k < j; k++)
    {
        brr[i][j] = min(brr[i][j], mcmultiplication(i, k) + mcmultiplication(k + 1, j) + arr[i - 1] * arr[k] * arr[j]);
    }

    return brr[i][j];
}

int main()
{
    int n;
    printf("Enter size ");
    scanf("%d", &n);

    printf("Enter dimensions\n");
    for (int i = 0; i < n; i++)
    {
        scanf("%d", &arr[i]);
    }

    for (int i = 0; i < 10; i++)
    {
        for (int j = 0; j < 10; j++)
        {
            brr[i][j] = -1;
        }
    }

    mcmultiplication(1, n - 1);
    for(int i=0; i < 10; i++)
    {
        for(int j = 0; j < 10; j++)
        {
            printf("%d ", brr[i][j]);
        }
    }
}
