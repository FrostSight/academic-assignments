#include<stdio.h>

int partitioning(int brr[], int low, int high)
{
    int key = brr[low], temp1, temp2;  //taking pivot as the first index
    int start = low;
    int end = high;

    while(start<end)
    {
        while(brr[start] <= key)  //moving forward
            start++;
        while(brr[end] > key)  //moving backward
            end--;
        if(start < end)  // only in this condition value of start index will exchange with the value of end index
        {
            temp1 = brr[start];
            brr[start] = brr[end];
            brr[end] = temp1;
        }
    }

    //i.e here start >end (upper while loop has broken) now value of end exchange with pivot
    temp2 = brr[low];
    brr[low] = brr[end];
    brr[end] = temp2;

    return end;
}

int quickSort(int brr[], int low, int high)
{
    int pos;
    if(low<high)
    {
        pos = partitioning(brr, low, high);   //receiving pivot

        quickSort(brr, low, pos-1);   // left side elements less than pivot
        quickSort(brr, pos+1, high);  // right side elements greater than pivot
    }
    else
        return;
}

int main()
{
    int size, i, high, low;
    printf("Enter array size ");
    scanf("%d", &size);

    int brr[size];
    printf("Enter elements\n");
    for(i=0; i<size; i++)
        scanf("%d", &brr[i]);

    printf("\nBefore sorting\n");
    for (i = 0; i < size; i++)
    {
        printf("%d ", brr[i]);
    }

    low = 0,high = size-1;
    quickSort(brr, low, high);

    printf("\nThe sorted array is below\n");
    for(i=low; i<=high; i++)
        printf("%d ", brr[i]);
    printf("\n");
}

/* Time Complexity
---------------------------------------
==> wrost case O(n^2)
==> best case (n log n)
---------------------------------------
*/
