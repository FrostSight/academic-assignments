#include <bits/stdc++.h>
using namespace std;

int node, edge, parent[10];
vector<vector<int>> graph;

int findings(int i)
{
    while (parent[i] != i)
        i = parent[i];

    return i;
}

void unions(int i, int j)
{
    int a = findings(i);
    int b = findings(j);

    parent[a] = b;
}

void mst()
{
    int edge_count = 0;
    while (edge_count < node - 1)
    {
        int minn = INT_MAX, a = -1, b = -1;
        for (int i = 0; i < node; i++)
        {
            for (int j = 0; j < node; j++)
            {
                if (findings(i) != findings(j) && graph[i][j] < minn)
                {
                    minn = graph[i][j];
                    a = i;
                    b = j;
                }
            }
        }
        unions(a, b);
        printf("Edge %d\n", edge_count);
    }
}

int main()
{
    cin >> node >> edge;

    for (int i = 0; i < edge; i++)
    {
        int u, v, w;
        cin >> u >> v >> w;
        graph.push_back({u, v, w});
    }
    for (int i = 0; i < node; i++)
        parent[i] = i;

    mst();
}
