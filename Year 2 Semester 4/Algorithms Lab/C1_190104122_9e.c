#include<stdio.h>
int main()
{
    int size, data, i;
    printf("Enter array size ");
    scanf("%d", &size);

    int arr[size];
    printf("Enter elements\n");
    for(i=0; i<size; i++)
    {
        scanf("%d", &arr[i]);
    }

    printf("Enter desired data ");
    scanf("%d", &data);

    int low=0, high = size-1, mid, flag=0, pos;
    mid = (low+high)/2;
    while(low<high)
    {
        if(data == arr[mid])
        {
            pos = mid+1;
            flag=1;
            break;
        }
        else if(data < arr[mid])
            high = mid-1;
        else
            low = mid+1;
    }
    if(flag==1)
        printf("\nData found at %dth position\n", pos);
    else
        printf("\nData not found\n");
}
