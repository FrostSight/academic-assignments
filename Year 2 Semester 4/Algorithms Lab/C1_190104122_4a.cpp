#include <bits/stdc++.h>
using namespace std;

int arr[10][10], counter=0;

int lcsubsequence(string &str1, string &str2, int size1, int size2)
{
    if (size1 == 0 || size2 == 0)
        return 0;

    else
    {
        counter++;

        if (str1[size1 - 1] == str2[size2 - 1])
            arr[size1][size2] = 1 + lcsubsequence(str1, str2, size1 - 1, size2 - 1); //diagonally upper back
        else
            arr[size1][size2] = max(lcsubsequence(str1, str2, size1, size2-1), lcsubsequence(str1, str2, size1-1, size2)); // max of one back and one up
    }

    printf("Longest length of subsequence is %d", counter);
    return arr[size1][size2];
}

int main()
{
    string str1, str2;
    printf("Enter sequence\n");
    scanf("%s%s", &str1, &str2);

    int size1 = str1.size();
    int size2 = str2.size();

    for (int i = 0; i < 10; i++)
    {
        for (int j = 0; j < 10; j++)
        {
            arr[i][j] = -1;
        }
    }

    lcsubsequence(str1, str2, size1, size2);
}
