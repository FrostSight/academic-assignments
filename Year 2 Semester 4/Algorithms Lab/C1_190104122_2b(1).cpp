#include <bits/stdc++.h>
using namespace std;

vector<int> graph[10];
int check[10];
vector<int> view;

void traversing(int x)
{
    check[x] = 1;
    for (int i = 0; i < graph[x].size(); i++)
    {
        int y = graph[x][i];

        if (check[y] == 0)
            traversing(y);
    }
    view.push_back(x);
}

int main()
{
    int node, edge, u, v;
    printf("Enter number of nodes ");
    scanf("%d", &node);
    printf("Enter number of edges ");
    scanf("%d", &edge);

    printf("Enter elements\n");
    for (int i = 0; i < edge; i++)
    {
        scanf("%d%d", &u, &v);
        graph[u].push_back(v);
    }

    printf("Enter a node to start traversing ");
    scanf("%d", &node);
    traversing(node);

    reverse(view.begin(), view.end());

    cout << "Topological sort is below\n";
    for (int i = 0; i < view.size(); i++)
        printf("%d ", view[i]);
}
