package com.example.assignment2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener
{
    private ImageView img1, img2, img3, img4, img5, img6, img7, img8;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        img1 = (ImageView) findViewById(R.id.imageView1);
        img2 = (ImageView) findViewById(R.id.imageView2);
        img3 = (ImageView) findViewById(R.id.imageView3);
        img4 = (ImageView) findViewById(R.id.imageView4);
        img5 = (ImageView) findViewById(R.id.imageView5);
        img6 = (ImageView) findViewById(R.id.imageView6);
        img7 = (ImageView) findViewById(R.id.imageView7);
        img8 = (ImageView) findViewById(R.id.imageView8);

        img1.setOnClickListener(this);
        img2.setOnClickListener(this);
        img3.setOnClickListener(this);
        img4.setOnClickListener(this);
        img5.setOnClickListener(this);
        img6.setOnClickListener(this);
        img7.setOnClickListener(this);
        img8.setOnClickListener(this);

    }

    @Override
    public void onClick(View v)
    {
        if (v.getId() == R.id.imageView1)
            Toast.makeText(MainActivity.this, "Travel", Toast.LENGTH_SHORT).show();

        else if (v.getId() == R.id.imageView2)
            Toast.makeText(MainActivity.this, "Report", Toast.LENGTH_SHORT).show();

        else if (v.getId() == R.id.imageView3)
            Toast.makeText(MainActivity.this, "Music", Toast.LENGTH_SHORT).show();

        else if (v.getId() == R.id.imageView4)
            Toast.makeText(MainActivity.this, "Movie", Toast.LENGTH_SHORT).show();

        else if (v.getId() == R.id.imageView5)
            Toast.makeText(MainActivity.this, "Gallery", Toast.LENGTH_SHORT).show();

        else if (v.getId() == R.id.imageView6)
            Toast.makeText(MainActivity.this, "Alarm", Toast.LENGTH_SHORT).show();

        else if (v.getId() == R.id.imageView7)
            Toast.makeText(MainActivity.this, "Map", Toast.LENGTH_SHORT).show();

        else
            Toast.makeText(MainActivity.this, "Setting", Toast.LENGTH_SHORT).show();
    }
}