package com.example.ass_3;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class HospitalRecViewAdapter extends RecyclerView.Adapter<HospitalRecViewAdapter.ViewHolder> {
    private ArrayList<Hospital> hospitals = new ArrayList<>();
    private Context context;

    public HospitalRecViewAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @org.jetbrains.annotations.NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @org.jetbrains.annotations.NotNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.hospital_list_view, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;


    }

    @Override
    public void onBindViewHolder(@NonNull @org.jetbrains.annotations.NotNull HospitalRecViewAdapter.ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        holder.txtName1.setText(hospitals.get(position).getName());
        holder.txtName2.setText(hospitals.get(position).getPhone());
        holder.parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("tel:" + hospitals.get(position).getPhone());
                Intent intent = new Intent(Intent.ACTION_DIAL, uri);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return hospitals.size();
    }


    public void setHospitals(ArrayList<Hospital> hospitals) {
        this.hospitals = hospitals;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private RelativeLayout parent;
        private TextView txtName1, txtName2;

        public ViewHolder(@NonNull @org.jetbrains.annotations.NotNull View itemView) {
            super(itemView);
            txtName1 = itemView.findViewById(R.id.hospitalName);
            txtName2 = itemView.findViewById(R.id.hospitalContact);

            parent = itemView.findViewById(R.id.parent);
        }
    }
}
