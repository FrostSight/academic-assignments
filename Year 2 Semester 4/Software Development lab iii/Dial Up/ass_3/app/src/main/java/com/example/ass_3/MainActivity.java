package com.example.ass_3;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private RecyclerView hospitalRecView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setTitle("Assignment 3");

        hospitalRecView = findViewById(R.id.hospitalRV);

        ArrayList<Hospital> hospitals = new ArrayList<>();
        hospitals.add(new Hospital("Labaid Specialized Hospital", "01521555771"));
        hospitals.add(new Hospital("Labaid Cardiac Hospital", "01321455432"));
        hospitals.add(new Hospital("United Hospital Limited", "01214001243"));
        hospitals.add(new Hospital("Evercare Hospital Dhaka", "07666714067"));
        hospitals.add(new Hospital("Square Hospital", "01610201061"));
        hospitals.add(new Hospital("Asgar Ali Hospital", "01712234567"));
        hospitals.add(new Hospital("ICU Specialized Hospital Limited", "012345632456"));
        hospitals.add(new Hospital("Renesa Hospital and Research Institute Limited", "01345676543"));
        hospitals.add(new Hospital("CKD & Urology Hospital (ICU & Nephrolo)", "017123454632"));
        hospitals.add(new Hospital("Dhaka paediatric-Neonatal & General Hospital", "01234564352"));
        hospitals.add(new Hospital("City Hospital & Diagonstic Center", "01456453241"));
        hospitals.add(new Hospital("Bangladesh Eye Hospital", "01256437622"));
        hospitals.add(new Hospital("Basundhura Hospital", "01251237622"));
        hospitals.add(new Hospital("Dhaka Medical College", "10606"));
        hospitals.add(new Hospital("CMH Hospital", "10890"));
        hospitals.add(new Hospital("Asgar Ali Hopital", "20876"));
        hospitals.add(new Hospital("BIHS Hospital", "90812"));
        hospitals.add(new Hospital("Islamic Bank Hospital", "88712"));
        hospitals.add(new Hospital("ICU Specialized Hospital Limited", "87653"));
        hospitals.add(new Hospital("CKD & Urology Hospital", "13452"));

        HospitalRecViewAdapter adapter = new HospitalRecViewAdapter(this);
        adapter.setHospitals(hospitals);

        hospitalRecView.setAdapter(adapter);
        hospitalRecView.setLayoutManager(new LinearLayoutManager(this));
    }
}