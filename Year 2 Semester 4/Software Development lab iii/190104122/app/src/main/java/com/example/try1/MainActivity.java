package com.example.try1;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener
{

    private EditText editText1, editText2;
    private ImageButton plus, minus, multiplication, division;
    private TextView rslt;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText1 = (EditText)findViewById(R.id.input1);
        editText2 = (EditText)findViewById(R.id.input2);

        plus = (ImageButton) findViewById(R.id.plus_btn);
        minus = (ImageButton) findViewById(R.id.minus_btn);
        multiplication = (ImageButton) findViewById(R.id.multiplication_btn);
        division = (ImageButton) findViewById(R.id.division_btn);

        rslt = (TextView)findViewById(R.id.rslt_view);

        plus.setOnClickListener(this);
        minus.setOnClickListener(this);
        multiplication.setOnClickListener(this);
        division.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {

            try
            {
                double num1 = Double.parseDouble(editText1.getText().toString());
                double num2 = Double.parseDouble(editText2.getText().toString());

                if (v.getId() == R.id.plus_btn)
                {

                    double sum = num1 + num2;
                    rslt.setText("Result = "+sum);
                }
                if (v.getId() == R.id.minus_btn)
                {
                    double sub = num1 - num2;
                    rslt.setText("Result = "+sub);
                }
                if (v.getId() == R.id.multiplication_btn)
                {
                    double mul = num1 * num2;
                    rslt.setText("Result = "+mul);
                }
                if (v.getId() == R.id.division_btn)
                {
                    double div = num1 / num2;
                    rslt.setText("Result = "+div);
                }
            }catch (Exception e)
            {
                Toast.makeText(MainActivity.this,"Invalid input",Toast.LENGTH_SHORT).show();
            }
    }
}