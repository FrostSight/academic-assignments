#include <stdio.h>
#include <string.h>

int i, pos, count = 0;
char functionn[10][10], lexem[1000], input[1000], output1[1000];

struct table
{
    int step;
    char name[10], identifier_type[10], data_type[10], scope[10], value[10];
};
struct table id[10];

void view_all();

int int_numberCheck(char numeric[])
{
    int a, point = 0;
    for (a = 0; numeric[a] != '\0'; a++)
    {
        if (numeric[a] >= 48 && numeric[a] <= 57)
            continue;
        else {return 0;}
    }
    return 1;
}

int numberCheck(char numeric[])
{
    int a, point = 0;
    for(a = 0; numeric[a] != '\0'; a++)
    {
        if((numeric[a] >= 48 && numeric[a] <= 57) || (numeric[a] == 46))
        {
            if(numeric[a] == 46) {point++;}

            if(point > 1) {return 0;}

            continue;
        }
        else{return 0;}
    }
    if(numeric[a-1] == 46) {return 0;}

    return 1;
}

int identifierCheck(int step_no)
{
    int a;
    if((id[step_no].name[0] >= 65 && id[step_no].name[0] <= 90) || (id[step_no].name[0] >= 97 && id[step_no].name[0] <= 122) || (id[step_no].name[0] == 95))
    {
        for(a = 1; id[step_no].name[a] != '\0'; a++)
        {
            if((id[step_no].name[a] >= 65 && id[step_no].name[a] <=90) || (id[step_no].name[a] >= 97 && id[step_no].name[a] <= 122) || (id[step_no].name[a] >= 48 && id[step_no].name[a] <= 57) || (id[step_no].name[a] == 95))
                continue;
            else {return 0;}
        }
        return 1;
    }
    else {return 0;}
}

int insertion(int step_no)
{
    int choice, j = 0;
    printf("\nstep : %d\n", step_no);
    id[step_no].step = step_no;
    printf("name : ");
    fflush(stdin);
    gets(id[step_no].name);
    if(!identifierCheck(step_no))
    {
        printf("Invalid input !!!");
        count--;
        return 0;
    }
    printf("identifier type : 1. variable 2. function\n??? ");
    scanf("%d", &choice);
    if(choice == 1)
        strcpy(id[step_no].identifier_type, "variable");
    else if (choice == 2)
        strcpy(id[step_no].identifier_type, "function");
    else
    {
        printf("Invalid option !!!");
        count--;
        return 0;
    }
    if(!strcmp(id[step_no].identifier_type, "function"))
        printf("data type : 1. integer\t2. float\t3. double\t4. long\t\t5. void ??? ");
    else
        printf("data_type : 1. integer\t2. float\t3. double\t4. long ??? ");
    scanf("%d", &choice);
    if(choice == 1)
        strcpy(id[step_no].data_type, "integer");
    else if(choice == 2)
        strcpy(id[step_no].data_type, "float");
    else if(choice == 3)
        strcpy(id[step_no].data_type, "double");
    else if(choice == 4)
        strcpy(id[step_no].data_type, "long");
    else if(choice == 5 && !strcmp(id[step_no].identifier_type, "function"))
        strcpy(id[step_no].data_type, "void");
    else
    {
        printf("Invalid option !!!");
        count--;
        return 0;
    }
    if(!strcmp(id[step_no].identifier_type, "function"))
    {
        printf("global\n");
        strcpy(id[step_no].scope, "global");
    }
    else
    {
        int m=0;
        for(j = 1; j <= count; j++)
        {
            if(!strcmp(id[j].identifier_type, "function"))
            {
                m++;
                printf(" %d. %s", m, id[j].name);
                strcpy(functionn[m], id[j].name);
            }
        }
        printf("  %d. global ??? ", m+1);
        strcpy(functionn[m+1], "global");
        scanf("%d", &choice);
        if(choice > j)
        {
            printf("Invalid option !!!");
            count--;
            return 0;
        }
        else
            strcpy(id[step_no].scope, functionn[choice]);
    }
    if(!strcmp(id[step_no].identifier_type, "function"))
        strcpy(id[step_no].value, "");
    else
    {
        printf("value : ");
        fflush(stdin);
        gets(id[step_no].value);
        if(!int_numberCheck(id[step_no].value) && (!strcmp(id[step_no].data_type, "integer") || !strcmp(id[step_no].data_type, "long")))
        {
            printf("Invalid input !!!");
            count--;
            return 0;
        }
        if(!numberCheck(id[step_no].value) && (!strcmp(id[step_no].data_type, "float") || !strcmp(id[step_no].data_type, "double")))
        {
            printf("Invalid input !!!");
            count--;
            return 0;
        }
    }
    return 0;
}

void search(int step_no)
{
    if(step_no > 0 && step_no < count)
    {
        printf("\nstep : %d\n", step_no);
        printf("name : %s\n", id[step_no].name);
        printf("id_type : %s\n", id[step_no].identifier_type);
        printf("data_type : %s\n", id[step_no].data_type);
        printf("scope : %s\n", id[step_no].scope);
        printf("value : %s\n", id[step_no].value);
    }
    else {printf("Not found");}
}

void delete_(int step_no)
{
    if(step_no > 0 && step_no <= count)
    {
        for(int j = step_no; j < count; j++)
        {
            strcpy(id[j].name, id[j+1].name);
            strcpy(id[j].identifier_type, id[j+1].identifier_type);
            strcpy(id[j].data_type, id[j+1].data_type);
            strcpy(id[j].scope, id[j+1].scope);
            strcpy(id[j].value, id[j+1].value);
        }
        count--;
    }
    else {printf("Not found");}
}

void view_all(int step_count)
{
    printf("\nSl.No.\t\tName\t\tIdentifier Type\t\tData Type\tScope\t\tValue\n");
    printf("------\t\t----\t\t----------------\t---------\t-----\t\t-----\n");
    for(int j = 1; j <= step_count; j++)
        printf("%d\t\t%s\t\t%s\t\t%s\t\t%s\t\t%s\n", id[j].step, id[j].name, id[j].identifier_type, id[j].data_type, id[j].scope, id[j].value);
}

int main()
{
    system("COLOR 70");
    int num_var = 0, identifier_found = 0, identifier_name = 0, keyword_found = 0, value_found = 0, scope_found = 0, i;
    char c;
    FILE *p1, *p2;

    p1 = fopen("input3.c", "r");
    p2 = fopen("output.txt","w");

    int left_par = 0, right_par = 0;

    if (!p1)
        printf("\nFile can't be opened!");
    else
    {
        printf("Sample Input : \n");
        while((c = fgetc(p1)) != EOF)
            printf("%c", c);
    }
    fclose(p1);
    p1 = fopen("input3.c", "r");
    if (!p1)
        printf("\nFile can't be opened!");
    else
    {
        pos=0;
        printf("\nStep 1 :: Output");
        while((c = fgetc(p1)) != EOF)
        {
            if(right_par == 1 && c == ' ')  // pore next part strart korsi so kaaj nai continue korsi
            {
                right_par = 0;
                continue;
            }
            lexem[pos] = c;
            if(c == '[')
                left_par = 1;
            if(c == ' ' && left_par == 1) // text skip korlam
            {
                lexem[pos+1] = '\0';
                if(strcmp("[id ", lexem)) //not equal to "id" // identifier chara onno kisu rakhbo na just etai rakhbo
                {
                    pos=1;
                    continue;
                }
            }
            if(left_par == 1 && c == ']')
            {
                lexem[pos+1] = '\0';
                strcat(output1, lexem);
                strcat(output1, " ");
                pos = -1;
                left_par = 0;
                right_par = 1;
            }
            pos++;
        }
    }
    fclose(p1);
    printf("\n%s\n", output1);
    pos=0;
    i = 0;

    printf("\nStep 2 :: Output");
    while(output1[i] != '\0')
    {
        c = output1[i];
        if(c == '[' || c == ']')
        {
            i++;
            continue;
        }
        input[pos] = c;
        pos++;
        i++;
    }

    input[pos] = '\0'; //printf("\n%s\n", input); bracket removed
    pos = 0;
    i = 0;
    char _identifier_name[10];
    int identifier_get = 0, j;
    int value_pos = 0;

    while(input[pos] != '\0')
    {
        if(input[pos] != ' ')
        {
            c = input[pos];
            lexem[i] = c;
        }
        else
        {
            lexem[i] = '\0';
            i = -1;
            if((!strcmp("float", lexem)) || (!strcmp("double", lexem)) || (!strcmp("int", lexem)) || (!strcmp("long", lexem)))
            {
                count++;
                keyword_found = 1;
                id[count].step = count;
                strcpy(id[count].data_type, lexem);
            }
            if(!strcmp("id", lexem) && keyword_found == 1)
            {
                identifier_found = 1;
                identifier_name = 1;
            }
            else if(identifier_name && identifier_found && keyword_found == 1)
            {
                strcpy(id[count].name, lexem);
                identifier_name = 0;
            }
            if(!strcmp("(", lexem) && keyword_found == 1 && identifier_found == 1)
            {
                identifier_found = 0;
                keyword_found = 0;
                strcpy(id[count].identifier_type, "function");
                strcpy(id[count].scope, "global");
            }
            else if((!strcmp("=", lexem) || !strcmp(";", lexem) || !strcmp(")", lexem)) && keyword_found == 1 && identifier_found == 1)
            {
                identifier_found = 0;
                keyword_found = 0;
                scope_found = 0;
                strcpy(id[count].identifier_type, "variable");
                for(int j = count-1; j != 0; j--)
                {
                    if(!strcmp(id[j].identifier_type, "function"))
                    {
                        strcpy(id[count].scope, id[j].name);
                        scope_found = 1;
                        break;
                    }
                }
                if(scope_found == 0)
                    strcpy(id[count].scope, "global");
            }
            if(!strcmp("id", lexem))
                identifier_get = 1;
            else if(identifier_get)
            {
                identifier_get = 0;
                strcpy(_identifier_name, lexem);
            }
            if(!strcmp("=", lexem))
                value_found = 1;
            if(value_found && strcmp("=", lexem))
            {
                value_found = 0; // int x; x = 5;
                for(j = 1; j <= count; j++)
                {
                    if(!strcmp(_identifier_name, id[j].name))
                        value_pos = id[j].step;
                }
                if(numberCheck(lexem))
                    strcpy(id[value_pos].value, lexem);
            }
        }
        i++;
        pos++;
    }

    view_all(count);

    count++;
    char option[10];
    int step_no = 0;
    printf("\nStep 3 :: Output");
A :
    printf("\nChoice an option : \n");
    printf("1. insert  2. update  3. delete  4. search  5. display  6. Exit ??? ");
    fflush(stdin);
    gets(option);
    if(!strcmp(option, "1"))
    {
        step_no = count;
        insertion(step_no);
        count++;
        goto A;
    }
    else if(!strcmp(option, "2"))
    {
        printf("\nEnter the step no. : ");
        scanf("%d", &step_no);
        insertion(step_no);
        goto A;
    }
    else if(!strcmp(option, "3"))
    {
        printf("\nEnter the step no. : ");
        scanf("%d", &step_no);
        delete_(step_no);
        goto A;
    }
    else if(!strcmp(option, "4"))
    {
        printf("\nEnter the step no. : ");
        scanf("%d", &step_no);
        search(step_no);
        goto A;
    }
    else if(!strcmp(option, "5"))
    {
        view_all(count-1);
        goto A;
    }
    else if(!strcmp(option, "6"))
    {
        exit(1);
        view_all(count-1);
    }
    else
        printf("Invalid input !!!\n");
    fclose(p2);
    printf("\n\n");
    return 1;
}
