/* Comment_and_space_remove */
#include<stdio.h>

FILE *f1, *f2;
char c, d, e;

void blockComment()
{
    while ((c = fgetc(f1)) != EOF)
    {
        if (c == '*')
        {
            c = fgetc(f1);
            if (c == '/')
            {
                c = fgetc(f1);
                return;
            }
        }
    }
}

void singleComment()
{
    while ((c = fgetc(f1)) != EOF)
    {
        if(c == '\n')
            return;
    }
}

int main()
{
    f1 = fopen("input1.c", "r");
    f2 = fopen("output.txt", "w");

    if(!f1)
        printf("File cannot be opened\n");
    else
    {
        while((c = fgetc(f1)) != EOF)
        {
            if(c == '/')
            {
                c = fgetc(f1);

                if(c == '*')
                    blockComment();
                else
                    singleComment();
            }

        if (c == '\n' || c == '  ')
            continue;


        fputc(c, f2);
        }
    }

    fclose(f1);
    fclose(f2);

    f2 = fopen("output.txt","r");
    while((c = fgetc(f2)) != EOF)
    {
        printf("%c", c);
    }
    printf("\n");
    fclose(f2);
}
