#include <stdio.h>
#include <string.h>

FILE *p1, *p2, *t1, *t2, *t3, *t4, *t5, *t6, *t7, *t8, *t9, *t10, *t11, *t12, *t13;

char c, func[10][10], input[1000], output1[1000],  lex[1000], prev_lex[100];

int line = 1, pos = 0, a = 0, m, i = 0, new_line = 0, space = 0, special_char = 0, comment=0, quot = 0, star = 0, slash = 0, if_found = 0, for_found = 0, colon_found = 0,
open_1st_par = 0, miss_1st_par_line = 0, open_2nd_par = 0, miss_2nd_par_line = 0, kw_found = 0, id_found = 0, num_var = 0, id_name = 0, key_found = 0, value_found = 0,
scope_found = 0, left_par = 0, right_par = 0,  i, pos, count = 0;

struct table
{
    int step;
    char name[10], identifier_type[10], data_type[10], scope[10],  value[10];
};
struct table id[10];
void view_all();

int keywordCheck()
{
    if(!strcmp("auto", lex))            return 1;  else if(!strcmp(lex, "double"))     return 1;  else if(!strcmp(lex, "int"))        return 1;  else if(!strcmp(lex, "struct"))     return 1;
    else if(!strcmp(lex, "break"))      return 1;  else if(!strcmp(lex, "else"))       return 1;  else if(!strcmp(lex, "long"))       return 1;  else if(!strcmp(lex, "switch"))     return 1;
    else if(!strcmp(lex, "case"))       return 1;  else if(!strcmp(lex, "register"))   return 1;  else if(!strcmp(lex, "typedef"))    return 1;  else if(!strcmp(lex, "char"))       return 1;
    else if(!strcmp(lex, "extern"))     return 1;  else if(!strcmp(lex, "return"))     return 1;  else if(!strcmp(lex, "union"))      return 1;  else if(!strcmp(lex, "continue"))   return 1;
    else if(!strcmp(lex, "for"))        return 1;  else if(!strcmp(lex, "signed"))     return 1;  else if(!strcmp(lex, "void"))       return 1;  else if(!strcmp(lex, "do"))         return 1;
    else if(!strcmp(lex, "if"))         return 1;  else if(!strcmp(lex, "static"))     return 1;  else if(!strcmp(lex, "while"))      return 1;  else if(!strcmp(lex, "default"))    return 1;
    else if(!strcmp(lex, "goto"))       return 1;  else if(!strcmp(lex, "sizeof"))     return 1;  else if(!strcmp(lex, "volatile"))   return 1;  else if(!strcmp(lex, "const"))      return 1;
    else if(!strcmp(lex, "float"))      return 1;  else if(!strcmp(lex, "short"))      return 1;  else if(!strcmp(lex, "unsigned"))   return 1;  else if(!strcmp(lex, "enum"))       return 1;
    else    return 0;
}

int identifierCheck()
{
    if((lex[0] >= 65 && lex[0] <= 90) || (lex[0] >= 97 && lex[0] <= 122) || (lex[0] == 95))
    {
        for(a = 1; lex[a] != '\0'; a++)
        {
            if((lex[a] >= 65 && lex[a] <=90) || (lex[a] >= 97 && lex[a] <= 122) || (lex[a] >= 48 && lex[a] <= 57) || (lex[a] == 95))
            {
                continue;
            }
            else
            {
                return 0;
            }
        }
        return 1;
    }
    else
    {
        return 0;
    }
}

int numberCheck()
{
    int point = 0;
    for(a = 0; lex[a] != '\0'; a++)
    {
        if((lex[a] >= 48 && lex[a] <= 57) || (lex[a] == 46))
        {
            if(lex[a] == 46)
                point++;

            if(point > 1)
                return 0;

            continue;
        }
        else
        {
            return 0;
        }
    }
    if(lex[a-1] == 46)
        return 0;

    return 1;
}

int operatorCheck()
{
    for(a = 0; lex[a] != '\0'; a++)
    {
        if((lex[a] == 37) || (lex[a] == 42) || (lex[a] == 43) || (lex[a] == 45) || (lex[a] == 47) || (lex[a] >= 60 && lex[a] <= 62))
        {
            continue;
        }
        else
        {
            return 0;
        }
    }
    return 1;
}

int separatorCheck()
{
    for(a = 0; lex[a] != '\0'; a++)
    {
        if((lex[a] == 34) || (lex[a] == 39) || (lex[a] == 44) || (lex[a] == 59))
        {
            continue;
        }
        else
        {
            return 0;
        }
    }
    return 1;
}

int parenthesesCheck()
{
    for(a = 0; lex[a] != '\0'; a++)
    {
        if((lex[a] == 40) || (lex[a] == 41) || (lex[a] == 91) || (lex[a] == 93) || (lex[a] == 123) || (lex[a] == 125))
        {
            continue;
        }
        else
        {
            return 0;
        }
    }
    return 1;
}

int floatNumberCheck(char numeric[])
{
    int a, point = 0;
    for(a = 0; numeric[a] != '\0'; a++)
    {
        if((numeric[a] >= 48 && numeric[a] <= 57) || (numeric[a] == 46))
        {
            if(numeric[a] == 46)
                point++;

            if(point > 1)
                return 0;

            continue;
        }
        else
        {
            return 0;
        }
    }
    if(numeric[a-1] == 46)
        return 0;
    return 1;
}

void view_all(int step_count)
{
    printf("\nSl.No.\t\tName\t\tId Type\t\tData Type\tScope\t\tValue\n");
    printf("------\t\t----\t\t-------\t\t---------\t-----\t\t-----\n");

    for(int j = 1; j <= step_count; j++)
    {
        printf("%d\t\t%s\t\t%s\t\t%s\t\t%s\t\t%s\n", id[j].step, id[j].name, id[j].identifier_type, id[j].data_type, id[j].scope, id[j].value);
    }
}

void Assignment1()
{
    p1 = fopen("input5.c", "r");
    t1 = fopen("output1.txt","w");
    t7 = fopen("output4.txt","w");

    printf("Assignment 1 :: Output\n");

    if (!p1)
    {
        printf("\nFile can't be opened!");
    }
    else
    {
        fprintf(t1, "1       ");
        while((c = fgetc(p1)) != EOF)
        {
            if(c == '(' || c == ')' || c == '{' || c == '}' || c == '>' || c == '<' || c == ',' || c == ';' || c == '=')
            {
                fprintf(t1, "      ");
                fprintf(t7, "        ");
                special_char = 1;
            }

            if(c == '\n')
            {
                line++;
                fprintf(t1, "\n%d         ", line);
                fprintf(t7, "     ");
                continue;
            }

            if(c == '/')
                slash++;

            if(c == '*')
                star++;

            if(slash == 0 && star == 0)
            {
                fprintf(t1, "%c", c);
                fprintf(t7, "%c", c);
            }

            if(c == '/' && star > 1)
            {
                slash = 0;
                star = 0;
            }

            if(special_char)
            {
                fprintf(t1, "              ");
                fprintf(t7, "              ");
                special_char = 0;
            }
        }
    }
    fclose(t1);
    fclose(t7);
    t2 = fopen("output1.txt","r");
    t3 = fopen("output2.txt","w");
    if (!t2)
    {
        printf("\nFile can't be opened!");
    }
    else
    {
        while((c = fgetc(t2)) != EOF)
        {
            if(c == ' ')
            {
                space++;
                continue;
            }
            if(c != ' ' && space > 0)
            {
                fprintf(t3, " ");
                space = 0;
            }
            fprintf(t3, "%c", c);
        }
    }
    fclose(t2);
    fclose(t3);
    int flag = 0;

    t8 = fopen("output4.txt","r");
    t9 = fopen("output5.txt","w");
    if (!t2)
    {
        printf("\nFile can't be opened!");
    }
    else
    {
        while((c = fgetc(t8)) != EOF)
        {
            if(c == ' ')
            {
                space++;
                continue;
            }
            if(c != ' ' && space > 0)
            {
                flag++;
                if(flag > 1)
                    fprintf(t9, " ");

                printf(" ");
                space = 0;
            }
            fprintf(t9, "%c", c);
            printf("%c", c);
        }
    }
    printf("\n\n\n");
    fclose(t8);
    fclose(t9);
}

void Assignment2()
{
    t4 = fopen("output2.txt","r");
    t5 = fopen("output3.txt","w");

    if (!t4)
    {
        printf("\nFile can't be opened!");
    }
    else
    {
        while((c = fgetc(t4)) != EOF)
        {
            if(c != ' ')
            {
                lex[i] = c;
            }
            else
            {
                lex[i] = '\0';
                i = -1;

                if(!keywordCheck() && identifierCheck())
                {
                    fprintf(t5, "id ");
                }
                fprintf(t5, "%s ", lex);
            }
            i++;
        }
    }
    fclose(t4);
    fclose(t5);

    t10 = fopen("output5.txt","r");
    t11 = fopen("output6.txt","w");

    if (!t10)
    {
        printf("\nFile can't be opened!");
    }
    else
    {
        pos=0;
        printf("\nAssignment 2 :: Output\n");
        while((c = fgetc(t10)) != EOF)
        {
            if(c != ' ')
            {
                lex[pos] = c;
            }
            else
            {
                lex[pos] = '\0';

                if(keywordCheck())
                {
                    printf("[kw %s] ", lex);
                    fprintf(t11, "[kw %s] ", lex);
                }
                else if(identifierCheck())
                {
                    printf("[id %s] ", lex);
                    fprintf(t11, "[id %s] ", lex);
                }
                else if(numberCheck())
                {
                    printf("[num %s] ", lex);
                    fprintf(t11, "[num %s] ", lex);
                }
                else if(operatorCheck())
                {
                    printf("[op %s] ", lex);
                    fprintf(t11, "[op %s] ", lex);
                }
                else if(separatorCheck())
                {
                    printf("[sep %s] ", lex);
                    fprintf(t11, "[sep %s] ", lex);
                }
                else if(parenthesesCheck())
                {
                    printf("[par %s] ", lex);
                    fprintf(t11, "[par %s] ", lex);
                }
                else
                {
                    printf("[unkn %s] ", lex);
                    fprintf(t11, "[unkn %s] ", lex);
                }
                pos=-1;
            }
            pos++;
        }
    }
    printf("\n\n\n");
    fclose(t10);
    fclose(t11);
}

void Assignment3()
{
    t12 = fopen("output6.txt","r");
    t13 = fopen("output7.txt","w");

    if (!t12)
    {
        printf("\nFile can't be opened!");
    }
    else
    {
        pos=0;
        while((c = fgetc(t12)) != EOF)
        {
            if(right_par == 1 && c == ' ')
            {
                right_par = 0;
                continue;
            }
            lex[pos] = c;
            if(c == '[')
            {
                left_par = 1;
            }
            if(c == ' ' && left_par == 1)
            {
                lex[pos+1] = '\0';
                if(strcmp("[id ", lex))
                {
                    pos=1;
                    continue;
                }
            }
            if(left_par == 1 && c == ']')
            {
                lex[pos+1] = '\0';
                strcat(output1, lex);
                strcat(output1, " ");
                pos = -1;
                left_par = 0;
                right_par = 1;
            }
            pos++;
        }
    }
    fclose(p1);
    pos=0;
    i = 0;

    printf("\nAssignment 3 :: Output");
    while(output1[i] != '\0')
    {
        c = output1[i];
        if(c == '[' || c == ']')
        {
            i++;
            continue;
        }
        input[pos] = c;
        pos++;
        i++;
    }

    input[pos] = '\0';
    pos = 0;
    i = 0;
    char _id_name[10];
    int id_get = 0, j;
    int value_pos = 0;

    while(input[pos] != '\0')
    {
        if(input[pos] != ' ')
        {
            c = input[pos];
            lex[i] = c;
        }
        else
        {
            lex[i] = '\0';
            i = -1;
            if((!strcmp("float", lex)) || (!strcmp("double", lex)) || (!strcmp("int", lex)) || (!strcmp("long", lex)))
            {
                count++;
                key_found = 1;
                id[count].step = count;
                strcpy(id[count].data_type, lex);
            }
            if(!strcmp("id", lex) && key_found == 1)
            {
                id_found = 1;
                id_name = 1;
            }
            else if(id_name && id_found && key_found == 1)
            {
                strcpy(id[count].name, lex);
                id_name = 0;
            }
            if(!strcmp("(", lex) && key_found == 1 && id_found == 1)
            {
                id_found = 0;
                key_found = 0;
                strcpy(id[count].identifier_type, "func");
                strcpy(id[count].scope, "global");
            }
            else if((!strcmp("=", lex) || !strcmp(";", lex) || !strcmp(")", lex)) && key_found == 1 && id_found == 1)
            {
                id_found = 0;
                key_found = 0;
                scope_found = 0;
                strcpy(id[count].identifier_type, "var");
                for(int j = count - 1; j != 0; j--)
                {
                    if(!strcmp(id[j].identifier_type, "func"))
                    {
                        strcpy(id[count].scope, id[j].name);
                        scope_found = 1;
                        break;
                    }
                }
                if(scope_found == 0)
                {
                    strcpy(id[count].scope, "global");
                }
            }
            if(!strcmp("id", lex))
            {
                id_get = 1;
            }
            else if(id_get)
            {
                id_get = 0;
                strcpy(_id_name, lex);
            }
            if(!strcmp("=", lex))
            {
                value_found = 1;
            }
            if(value_found && strcmp("=", lex))
            {
                value_found = 0;
                for(j = 1; j <= count; j++)
                {
                    if(!strcmp(_id_name, id[j].name))
                    {
                        value_pos = id[j].step;
                    }
                }
                if(floatNumberCheck(lex))
                    strcpy(id[value_pos].value, lex);
            }
        }
        i++;
        pos++;
    }

    view_all(count);

    fclose(t13);
}

void Assignment4()
{
    t6 = fopen("output3.txt","r");
    p2 = fopen("output.txt","w");

    strcpy(prev_lex, " ");
    line = 1;

    printf("\n\n\nAssignment 4 :: Output\n");
    if (!t6)
    {
        printf("\nFile can't be opened!");
    }
    else
    {
        i = 0;
        int point = 0, valid_id = 0;
        char id_list[10][10];
        while((c = fgetc(t6)) != EOF)
        {
            if(c != ' ')
            {
                lex[i] = c;
            }
            else
            {
                lex[i] = '\0';
                i = -1;

                if(keywordCheck() && !kw_found)
                {
                    kw_found = 1;
                }
                if(kw_found && !keywordCheck() && identifierCheck() && strcmp("id", lex))
                {
                    kw_found = 0;
                    strcpy(id_list[point], lex);
                    point++;
                }

                if(!keywordCheck() && identifierCheck() && strcmp("id", lex))
                {
                    for(int m = 0; m <= point; m++)
                    {
                        if(!strcmp(id_list[m], lex))
                        {
                            valid_id = 1;
                            break;
                        }
                    }
                    if(!valid_id)
                    {
                        printf(" Undeclared id %s in line no: %d\n", lex, line);
                    }
                    else
                        valid_id = 0;
                }

                if(!strcmp("(", lex))
                {
                    open_1st_par++;
                }
                if(!strcmp(")", lex))
                {
                    open_1st_par--;
                    miss_1st_par_line = line;
                }
                if(!strcmp("{", lex))
                {
                    open_2nd_par++;
                }
                if(!strcmp("}", lex))
                {
                    open_2nd_par--;
                    miss_2nd_par_line = line;
                }

                if(!strcmp("for", lex))
                {
                    for_found = 1;
                }
                if(!strcmp(";", lex) && for_found)
                {
                    colon_found++;
                    continue;
                }
                if(colon_found == 2 && for_found)
                {
                    colon_found = 0;
                    for_found = 0;
                }
                if(!strcmp("if", lex))
                {
                    if_found = 1;
                }
                if(!strcmp("else", lex) && if_found)
                {
                    if_found = 0;
                }
                else if(!strcmp("else", lex) && !if_found)
                {
                    printf(" Unnecessary %s in line no: %d\n", lex, line);
                }

                if(!strcmp(prev_lex, lex))
                {
                    printf(" Unnecessary %s in line no: %d\n", lex, line);

                }
                strcpy(prev_lex, lex);
            }
            if(c == '\n')
            {
                line++;
            }
            i++;
        }
    }
    if(open_1st_par > 0)
    {
        printf(" ) missing in line no: %d\n", miss_1st_par_line);
    }
    if(open_1st_par < 0)
    {
        printf(" ( missing in line no: %d\n", miss_1st_par_line);
    }
    if(open_2nd_par > 0)
    {
        printf(" } missing in line no: %d\n", miss_1st_par_line);
    }
    if(open_2nd_par < 0)
    {
        printf(" { missing in line no: %d\n", miss_1st_par_line);
    }

    fclose(t6);
    fclose(p1);
    fclose(p2);
}

int main(void)
{
        p1 = fopen("input5.c", "r");
        while((c = fgetc(p1)) != EOF)
        {
            printf("%c", c);
        }
        printf("\n");

        fclose(p1);

    Assignment1();
    Assignment2();
    Assignment3();
    Assignment4();

    return 0;
}
