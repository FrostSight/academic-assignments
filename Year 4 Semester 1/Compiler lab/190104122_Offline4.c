#include <stdio.h>
#include <string.h>

char prev_lex[100];
char lex[100];

int keywordCheck()
{
    if(!strcmp("auto", lex))            return 1;  else if(!strcmp(lex, "double"))     return 1;  else if(!strcmp(lex, "int"))        return 1;  else if(!strcmp(lex, "struct"))     return 1;
    else if(!strcmp(lex, "break"))      return 1;  else if(!strcmp(lex, "else"))       return 1;  else if(!strcmp(lex, "long"))       return 1;  else if(!strcmp(lex, "switch"))     return 1;
    else if(!strcmp(lex, "case"))       return 1;  else if(!strcmp(lex, "register"))   return 1;  else if(!strcmp(lex, "typedef"))    return 1;  else if(!strcmp(lex, "char"))       return 1;
    else if(!strcmp(lex, "extern"))     return 1;  else if(!strcmp(lex, "return"))     return 1;  else if(!strcmp(lex, "union"))      return 1;  else if(!strcmp(lex, "continue"))   return 1;
    else if(!strcmp(lex, "for"))        return 1;  else if(!strcmp(lex, "signed"))     return 1;  else if(!strcmp(lex, "void"))       return 1;  else if(!strcmp(lex, "do"))         return 1;
    else if(!strcmp(lex, "if"))         return 1;  else if(!strcmp(lex, "static"))     return 1;  else if(!strcmp(lex, "while"))      return 1;  else if(!strcmp(lex, "default"))    return 1;
    else if(!strcmp(lex, "goto"))       return 1;  else if(!strcmp(lex, "sizeof"))     return 1;  else if(!strcmp(lex, "volatile"))   return 1;  else if(!strcmp(lex, "const"))      return 1;
    else if(!strcmp(lex, "float"))      return 1;  else if(!strcmp(lex, "short"))      return 1;  else if(!strcmp(lex, "unsigned"))   return 1;  else if(!strcmp(lex, "enum"))       return 1;
    else    return 0;
}

int identifierCheck()
{
    int a;
    if((lex[0] >= 65 && lex[0] <= 90) || (lex[0] >= 97 && lex[0] <= 122) || (lex[0] == 95))
    {
        for(a = 1; lex[a] != '\0'; a++)
        {
            if((lex[a] >= 65 && lex[a] <=90) || (lex[a] >= 97 && lex[a] <= 122) || (lex[a] >= 48 && lex[a] <= 57) || (lex[a] == 95)){continue;}
            else{return 0;}
        }
        return 1;
    }
    else{return 0;}
}

int main(void)
{
    FILE *p1, *p2, *t1, *t2, *t3, *t4, *t5, *t6;
    char c;
    int line = 1, pos = 0, i = 0, new_line = 0, space = 0, special_char = 0, star = 0, slash = 0;

    p1 = fopen("input4.c", "r");
    t1 = fopen("output1.txt","w");

    if (!p1)
    {
        printf("\nFile can't be opened!");
    }
    else
    {
        fprintf(t1, "1    ");
        while((c = fgetc(p1)) != EOF)
        {
            if(c == '(' || c == ')' || c == '{' || c == '}' || c == '>' || c == '<' || c == ',' || c == ';' || c == '=')
            {
                fprintf(t1, "              ");
                special_char = 1;
            }
             if(special_char)
            {
                fprintf(t1, "   ");
                special_char = 0;
            }
            if(c == '\n')
            {
                line++;
                fprintf(t1, "\n%d         ", line);
                continue;
            }
            if(c == '/') {slash++;}
            if(c == '*') {star++;}

            if(slash == 0 && star == 0) {fprintf(t1, "%c", c);}

            if(c == '/' && star > 1)
            {
                slash = 0;
                star = 0;
            }
        }
    }
    fclose(t1);
    t2 = fopen("output1.txt","r");
    t3 = fopen("output2.txt","w");

    if (!t2)
        printf("\nFile can't be opened!");
    else
    {
        while((c = fgetc(t2)) != EOF)
        {
            if(c == ' ')
            {
                space++;
                continue;
            }
            if(c != ' ' && space > 0)
            {
                fprintf(t3, " ");
                space = 0;
            }
            fprintf(t3, "%c", c);
        }

    }
    fclose(t2);
    fclose(t3);
    t4 = fopen("output2.txt","r");
    t5 = fopen("output3.txt","w");

    if (!t4)
        printf("\nFile can't be opened!");
    else
    {
        while((c = fgetc(t4)) != EOF)
        {
            if(c != ' ') {lex[i] = c;}
            else
            {
                lex[i] = '\0';
                i = -1;

                if(!keywordCheck() && identifierCheck())
                    fprintf(t5, "id ");
                fprintf(t5, "%s ", lex);
            }
            i++;
        }
    }
    fclose(t4);
    fclose(t5);
    t6 = fopen("output3.txt","r");
    p2 = fopen("output.txt","w");

    strcpy(prev_lex, " ");
    line = 1;
    int if_found = 0, for_found = 0, colon_found = 0, open_1st_par = 0, miss_1st_par_line = 0, open_2nd_par = 0, miss_2nd_par_line = 0, kw_found = 0, id_found = 0;
    if (!t6)
        printf("\nFile can't be opened!");
    else
    {
        i = 0;
        int point = 0, valid_id = 0;
        char id_list[10][10];
        while((c = fgetc(t6)) != EOF)
        {
            if(c != ' ') {lex[i] = c;}
            else
            {
                lex[i] = '\0';
                i = -1;

                if(keywordCheck() && !kw_found)
                    kw_found = 1;

                if(kw_found && !keywordCheck() && identifierCheck() && strcmp("id", lex))
                {
                    kw_found = 0;
                    strcpy(id_list[point], lex);
                    point++;
                }

                if(!keywordCheck() && identifierCheck() && strcmp("id", lex))
                {
                    for(int m = 0; m <= point; m++)
                    {
                        if(!strcmp(id_list[m], lex))
                        {
                            valid_id = 1;
                            break;
                        }
                    }
                    if(!valid_id)
                        printf(" Undeclared id %s in line no: %d\n", lex, line);
                    else
                        valid_id = 0;
                }

                if(!strcmp("(", lex))
                    open_1st_par++;

                if(!strcmp(")", lex))
                {
                    open_1st_par--;
                    miss_1st_par_line = line;
                }
                if(!strcmp("{", lex))
                    open_2nd_par++;

                if(!strcmp("}", lex))
                {
                    open_2nd_par--;
                    miss_2nd_par_line = line;
                }

                if(!strcmp("for", lex))
                    for_found = 1;

                if(!strcmp(";", lex) && for_found)
                {
                    colon_found++;
                    continue;
                }
                if(colon_found == 2 && for_found)
                {
                    colon_found = 0;
                    for_found = 0;
                }
                if(!strcmp("if", lex))
                    if_found = 1;

                if(!strcmp("else", lex) && if_found)
                    if_found = 0;

                else if(!strcmp("else", lex) && !if_found)
                    printf(" Unnecessary %s in line no: %d\n", lex, line);

                if(!strcmp(prev_lex, lex))
                    printf(" Unnecessary %s in line no: %d\n", lex, line);

                strcpy(prev_lex, lex);
            }
            if(c == '\n') {line++;}
            i++;
        }
    }
    if(open_1st_par > 0)
        printf(" ) missing in line no: %d\n", miss_1st_par_line);

    if(open_1st_par < 0)
        printf(" ( missing in line no: %d\n", miss_1st_par_line);

    if(open_2nd_par > 0)
        printf(" } missing in line no: %d\n", miss_1st_par_line);

    if(open_2nd_par < 0)
        printf(" { missing in line no: %d\n", miss_1st_par_line);

    fclose(t6);
    fclose(p1);
    fclose(p2);
    return 0;
}
