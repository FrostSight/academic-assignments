/*Lexical_Analysis*/
#include <bits/stdc++.h>
char c;
FILE *f1, *f2, *f3, *f4;
bool isDelimiter(char c)
{
    if (c == '+' || c == '-' || c  == '*' || c == '/' || c == ',' || c == ';' || c == '>' || c == '<' || c == '=' || c == '(' || c == ')' || c == '[' || c == ']' || c == '{' || c == '}')
        return (true);
    return (false);
}

bool isOperator(char ch)
{
    if (ch == '+' || ch == '-' || ch == '*' || ch == '/' || ch == '>' || ch == '<' || ch == '=')
        return (true);
    return (false);
}

bool isSeparator(char ch)
{
    if(ch == ',' || ch == ';' || ch == '\\' || ch == '\'')
        return (true);
    return (false);
}

bool isParentheses(char ch)
{
    if(ch == '(' || ch == ')' || ch == '{' || ch == '}' || ch == '[' || ch == ']')
        return (true);
    return (false);
}

bool isKeyword(char* str)
{
    if (!strcmp(str, "if") || !strcmp(str, "else") ||
            !strcmp(str, "while") || !strcmp(str, "do") ||
            !strcmp(str, "break") ||
            !strcmp(str, "continue") || !strcmp(str, "int")
            || !strcmp(str, "double") || !strcmp(str, "float")
            || !strcmp(str, "return") || !strcmp(str, "char")
            || !strcmp(str, "case") || !strcmp(str, "char")
            || !strcmp(str, "sizeof") || !strcmp(str, "long")
            || !strcmp(str, "short") || !strcmp(str, "typedef")
            || !strcmp(str, "switch") || !strcmp(str, "unsigned")
            || !strcmp(str, "void") || !strcmp(str, "static")
            || !strcmp(str, "struct") || !strcmp(str, "goto"))
        return (true);
    return (false);
}

bool validIdentifier(char* str)
{
    if (isdigit(str[0]) || isDelimiter(str[0]) == true)
        return (false);
    return (true);
}

bool isRealNumber(char* ch)
{
    bool flag = true;
    int k = 0;
    while(ch[k] != '\0')
    {
        if(!isdigit(ch[k]))
        {
            if(ch[k] != '.')
                flag = false;
        }
        k++;
    }
    return (flag);
}

int main()
{
    char arr[500];

    f1 = fopen("input2.c", "r");
    f2 = fopen("output1.txt", "w");

    if(!f1)
        printf("File cannot be opened\n");
    else
    {
        while((c = fgetc(f1)) != EOF)
        {
            if (isDelimiter(c))
            {
                char next = fgetc(f1);
                if(next != ' ')
                {
                    fputc(' ',f2);
                    fputc(c, f2);
                    fputc(' ', f2);
                    fputc(next, f2);
                }
                else
                {
                    fputc(' ',f2);
                    fputc(c, f2);
                    fputc(next, f2);
                }
            }
            else
                fputc(c, f2);
        }
    }
    fclose(f1);
    fclose(f2);

    f3 = fopen("output1.txt","r");
    f4 = fopen("output2.txt","w");
    if(!f3)
        printf("File cannot be opened\n");
    else
    {
        while((c = fgetc(f3)) != EOF)
        {
            arr[0] = c;
            int i = 1;
            while((c = fgetc(f3)) != ' ')
            {
                arr[i] = c;
                i++;
            }
            arr[i] = '\0';

            if(isOperator(arr[0]))
            {
                fputs("[op ", f4);
                fputc(arr[0], f4);
                fputs("]", f4);
            }
            else if(isSeparator(arr[0]))
            {
                fputs("[sep ", f4);
                fputc(arr[0], f4);
                fputs("]", f4);
            }
            else if(isParentheses(arr[0]))
            {
                fputs("[par ", f4);
                fputc(arr[0], f4);
                fputs("]", f4);
            }
            else if(isKeyword(arr))
            {
                fputs("[kw ", f4);
                fputs(arr, f4);
                fputs("]", f4);
            }
            else if (isRealNumber(arr))
            {
                fputs("[num ", f4);
                fputs(arr, f4);
                fputs("]", f4);
            }
            else if(validIdentifier(arr))
            {
                fputs("[id ", f4);
                fputs(arr, f4);
                fputs("]", f4);
            }
            else
            {
                fputs("[unkn ", f4);
                fputs(arr, f4);
                fputs("]", f4);
            }
        }
    fclose(f3);
    fclose(f4);

    printf("input\n");
    f1 = fopen("input2.c", "r");
    while((c = fgetc(f1)) != EOF)
    {
        printf("%c", c);
    }
    printf("\n");

    fclose(f1);

    printf("\nResult\n");
    f2 = fopen("output2.txt","r");
    while((c = fgetc(f2)) != EOF)
    {
        printf("%c", c);
    }
    printf("\n");

    fclose(f2);
    }
}
