/* Line_numbers */
#include<stdio.h>
int main()
{
    FILE *f1, * f2;
    char c;
    int line = 1;

    f1 = fopen("Test1.c", "r");
    f2 = fopen("Testing.txt", "w");

    if (!f1)
        printf("File can not be opened\n");
    else
    {
        if((c = fgetc(f1)) != EOF)
        {
            fputc(line+'0', f2);
            fputc(':', f2);
            fputc(' ', f2);
        }

        while((c = fgetc(f1)) != EOF)
        {
            fputc(c, f2);
            if(c == '\n')
            {
                line++;
                fputc(line+'0', f2);
                fputc(':', f2);
                fputc(' ', f2);
            }
        }
    }

    fclose(f1);
    fclose(f2);

    f2 = fopen("Testing.txt", "r");

    while((c = fgetc(f2)) != EOF)
        printf("%c", c);

    fclose(f2);
}
