CLEAR SCREEN;
SET SERVEROUTPUT ON;
SET VERIFY OFF;

clear screen;
drop table ARTIST CASCADE CONSTRAINTS;
drop table ALBUM CASCADE CONSTRAINTS;
drop table TRACK CASCADE CONSTRAINTS;

create table ARTIST(
    artistId int,
    name varchar2(05),
    age int,
    gender varchar2(05),
    PRIMARY KEY(artistId));

create table ALBUM(
    albumId int,
	albumTitle varchar2(8),
	artistId int,
	certification varchar2(10),
	numberOfTracks int,
	PRIMARY KEY(albumId),
	FOREIGN KEY(artistId) REFERENCES ARTIST(artistId));
	
create table TRACK(
    trackID int,
	trackTitle VARCHAR2(8),
	genre VARCHAR2(9),
	albumID int,
	released date,
	PRIMARY KEY(trackID),
	FOREIGN KEY(albumID) REFERENCES ALBUM(albumID));

CREATE OR REPLACE TRIGGER TRIGGER_1
AFTER INSERT ON ARTIST
DECLARE
BEGIN
	DBMS_OUTPUT.PUT_LINE('INSERTED INTO ARTIST');
END;
/

CREATE OR REPLACE TRIGGER TRIGGER_2
AFTER INSERT ON ALBUM
DECLARE
BEGIN
	DBMS_OUTPUT.PUT_LINE('INSERTED INTO ALBUM');
END;
/

CREATE OR REPLACE TRIGGER TRIGGER_3
AFTER INSERT ON TRACK
DECLARE
BEGIN
	DBMS_OUTPUT.PUT_LINE('INSERTED INTO TRACK');
END;
/
	
insert into ARTIST(artistId,name,age,gender) values(1,'A',20,'M');
insert into ARTIST(artistId,name,age,gender) values(2,'B',30,'F');
insert into ARTIST(artistId,name,age,gender) values(3,'C',35,'M');
insert into ARTIST(artistId,name,age,gender) values(4,'D',32,'F');
insert into ARTIST(artistId,name,age,gender) values(5,'E',46,'F');
insert into ARTIST(artistId,name,age,gender) values(6,'F',48,'M');

insert into ALBUM (albumId,albumTitle,artistId,certification,numberOfTracks) values(1,'PONM',2,'GOLD',7);
insert into ALBUM (albumId,albumTitle,artistId,certification,numberOfTracks) values(2,'TSRQ',5,'PLATINUM',14);
insert into ALBUM (albumId,albumTitle,artistId,certification,numberOfTracks) values(3,'DCBA',3,'SILVER',12);
insert into ALBUM (albumId,albumTitle,artistId,certification,numberOfTracks) values(4,'HGFE',1,'GOLD',10);
insert into ALBUM (albumId,albumTitle,artistId,certification,numberOfTracks) values(5,'LKJI',2,'PLATINUM',9);

insert into TRACK (trackID,trackTitle,genre,albumID,released) values(1,'title1','rock',1,'22-jun-1992');
insert into TRACK (trackID,trackTitle,genre,albumID,released) values(2,'title2','rock',1,'12-jul-1992');
insert into TRACK (trackID,trackTitle,genre,albumID,released) values(3,'title3','country',4,'02-feb-1972');
insert into TRACK (trackID,trackTitle,genre,albumID,released) values(4,'title4','rock',1,'12-jul-1992');
insert into TRACK (trackID,trackTitle,genre,albumID,released) values(5,'title5','country',2,'25-mar-1997');
insert into TRACK (trackID,trackTitle,genre,albumID,released) values(6,'title6','country',2,'25-mar-1997');
insert into TRACK (trackID,trackTitle,genre,albumID,released) values(7,'title7','pop',3,'26-jun-2010');
insert into TRACK (trackID,trackTitle,genre,albumID,released) values(8,'title8','pop',5,'22-may-1982');

ACCEPT X NUMBER PROMPT "ENTER THE ARTIST ID : "

SET SERVEROUTPUT ON;

CREATE OR REPLACE PACKAGE ASSIGNMENT3_190104122 AS

	PROCEDURE SEARCHING(ID IN NUMBER);
	
	FUNCTION OTHER_ARTISTS(ID IN NUMBER, GENDER_ IN VARCHAR2)
	RETURN NUMBER;
	
	PROCEDURE NOT_EXIST;
	
END ASSIGNMENT3_190104122;
/

CREATE OR REPLACE PACKAGE BODY ASSIGNMENT3_190104122 AS

	PROCEDURE SEARCHING(ID IN NUMBER)
	IS 
	FOUND_ NUMBER;
	NOT_FOUND EXCEPTION;
	BEGIN
		FOUND_ := 0;
		FOR M IN (SELECT * FROM ARTIST WHERE ARTISTID = ID) LOOP
			IF(ID = M.ARTISTID) THEN
				DBMS_OUTPUT.PUT_LINE('AGE OF ' || M.NAME || ': ' || M.AGE);
				DBMS_OUTPUT.PUT_LINE('GENDER OF ' || M.NAME || ': ' || M.GENDER);
				FOUND_ := OTHER_ARTISTS(ID, M.GENDER);
			END IF;	
		END LOOP;
		IF(FOUND_ = 0) THEN
			RAISE NOT_FOUND;
		END IF;
	
	EXCEPTION
		WHEN NOT_FOUND THEN
			NOT_EXIST;
	END SEARCHING;	
	
	FUNCTION OTHER_ARTISTS(ID IN NUMBER, GENDER_ IN VARCHAR2)
	RETURN NUMBER
	IS 
	TEMP NUMBER;
	BEGIN
	TEMP := 0;
		FOR N IN (SELECT NAME, GENDER, ALBUMTITLE, GENRE, NUMBEROFTRACKS FROM
				((SELECT COUNT(GENRE), ALBUMID FROM TRACK GROUP BY ALBUMID) NATURAL JOIN
				 (SELECT * FROM (SELECT GENRE, ALBUMID FROM TRACK) NATURAL JOIN
				 (SELECT * FROM ARTIST NATURAL JOIN ALBUM WHERE GENDER = GENDER_ 
				 AND ARTISTID != ID AND (CERTIFICATION = 'GOLD' OR CERTIFICATION = 'PLATINUM'))))) LOOP

				TEMP := 1;
				DBMS_OUTPUT.PUT_LINE('OTHER ARTISTS OF ' || GENDER_ || ' GENDER: ' || N.NAME);
				DBMS_OUTPUT.PUT_LINE('ALBUMS OF ' || N.NAME || ': ' || N.ALBUMTITLE);
				DBMS_OUTPUT.PUT_LINE('GENRE OF ' || N.ALBUMTITLE || ': ' || N.GENRE);
				DBMS_OUTPUT.PUT_LINE('NUMBER OF TRACKS IN ' || N.ALBUMTITLE || ': ' || N.NUMBEROFTRACKS);
				
				RETURN 1;	
				
		END LOOP;
		IF(TEMP = 0) THEN
			DBMS_OUTPUT.PUT_LINE('NO ANY OTHER ' || GENDER_|| '(GENDER) ARTISTS HAVING GOLD OR, PLATINUM CERTIFICATION');
			RETURN 1;
		END IF;
		
	END OTHER_ARTISTS;	
	
	PROCEDURE NOT_EXIST
	IS
	
	BEGIN
		DBMS_OUTPUT.PUT_LINE('ARTIST does not exist');
	END NOT_EXIST;
	
	
END ASSIGNMENT3_190104122;
/

DECLARE
	ID number;
BEGIN
	ID := &X;
	ASSIGNMENT3_190104122.SEARCHING(ID);

END;
/

commit;