CLEAR SCREEN;

--1. Create the four tables and insert necessary data in the tables using SQL commands.
DROP TABLE Student;
DROP TABLE Borrows;
DROP TABLE Book;


CREATE TABLE Student(studentID INTEGER, name VARCHAR2(25), phone VARCHAR2(15), age INTEGER);

INSERT INTO Student VALUES(1001, 'Ali Hasan', '01800000000' , 18);
INSERT INTO Student VALUES(1002, 'Ayesha', '01600000000', 17);
INSERT INTO Student VALUES(1003, 'Elias', '01800000001', 19);
INSERT INTO Student VALUES(1004, 'Fatima', '01800000002', 18);
INSERT INTO Student VALUES(1005, 'Yousuf', '01800002000', 17);
INSERT INTO Student VALUES(1006, 'Salina', '01802200000', 18);
INSERT INTO Student VALUES(1007, 'Kaysar', '01803230000', 16);
INSERT INTO Student VALUES(1008, 'Sadia', '01800000111', 19);
INSERT INTO Student VALUES(1009, 'Shuvo', '01812340000', 18);
INSERT INTO Student VALUES(1010, 'Maria', '01800876000', 17);


CREATE TABLE Borrows(studentID INTEGER, bookID INTEGER, dateBorrowed date);

INSERT INTO Borrows VALUES(1001, 101, '17-Jan-2017');
INSERT INTO Borrows VALUES(1001, 102, '20-Jan-2017');
INSERT INTO Borrows VALUES(1002, 103, '17-Jan-2017');
INSERT INTO Borrows VALUES(1001, 104, '17-Jan-2017');
INSERT INTO Borrows VALUES(1001, 105, '20-Jan-2017');
INSERT INTO Borrows VALUES(1003, 106, '17-Jan-2017');
INSERT INTO Borrows VALUES(1003, 107, '20-Jan-2017');
INSERT INTO Borrows VALUES(1004, 108, '17-Jan-2017');
INSERT INTO Borrows VALUES(1004, 109, '20-Jan-2017');
INSERT INTO Borrows VALUES(1004, 110, '17-Jan-2017');
INSERT INTO Borrows VALUES(1004, 111, '20-Jan-2017');
INSERT INTO Borrows VALUES(1005, 112, '17-Jan-2017');
INSERT INTO Borrows VALUES(1006, 113, '20-Jan-2017');
INSERT INTO Borrows VALUES(1007, 114, '17-Jan-2017');
INSERT INTO Borrows VALUES(1004, 115, '20-Jan-2017');



CREATE TABLE Book(bookID INTEGER, authorID INTEGER, title VARCHAR2(25), genre VARCHAR2(25));

INSERT INTO Book VALUES(101, 11, 'A', 'History');
INSERT INTO Book VALUES(102, 12, 'B', 'Science Fiction');
INSERT INTO Book VALUES(103, 13, 'C', 'Mystery');
INSERT INTO Book VALUES(104, 11, 'A', 'History');
INSERT INTO Book VALUES(105, 12, 'B', 'Science Fiction');
INSERT INTO Book VALUES(106, 13, 'C', 'Mystery');
INSERT INTO Book VALUES(107, 11, 'A', 'History');
INSERT INTO Book VALUES(108, 12, 'B', 'Science Fiction');
INSERT INTO Book VALUES(109, 13, 'C', 'Mystery');
INSERT INTO Book VALUES(110, 11, 'A', 'History');
INSERT INTO Book VALUES(111, 12, 'B', 'Science Fiction');
INSERT INTO Book VALUES(112, 13, 'C', 'Mystery');
INSERT INTO Book VALUES(113, 11, 'A', 'History');
INSERT INTO Book VALUES(114, 12, 'B', 'Science Fiction');
INSERT INTO Book VALUES(115, 13, 'C', 'Mystery');
INSERT INTO Book VALUES(116, 11, 'A', 'History');
INSERT INTO Book VALUES(117, 12, 'B', 'Science Fiction');
INSERT INTO Book VALUES(118, 13, 'C', 'Mystery');
INSERT INTO Book VALUES(119, 11, 'A', 'History');
INSERT INTO Book VALUES(120, 10, 'B', 'Science Fiction');


SELECT * FROM Student;
SELECT * FROM Borrows;
SELECT * FROM Book;


--2_a. Find the authorID of the author who published at least 5 books.
SELECT authorID 
FROM (SELECT COUNT(authorID) AS No_Of_Books, authorID 
		FROM Book 
		GROUP BY authorID)
WHERE No_Of_Books >= 5;


--2_b. Find the name of the student who borrowed the highest number of books in January 2017.
SELECT name 
FROM Student 
WHERE studentID = (SELECT studentID 
                FROM (SELECT COUNT(studentID) AS Total_Borrowed, studentID 
                    FROM (SELECT studentID, bookID 
                        FROM Borrows 
                        WHERE dateBorrowed >= '01-Jan-2017' AND dateBorrowed <= '31-Jan-2017') 
                    GROUP BY studentID) 
                WHERE Total_Borrowed = (SELECT MAX(Total_Borrowed) 
                                        FROM (SELECT COUNT(studentID) AS Total_Borrowed, studentID 
                                            FROM (SELECT studentID, bookID 
                                                FROM Borrows 
                                                WHERE dateBorrowed >= '01-Jan-2017' AND dateBorrowed <= '31-Jan-2017')
                                            GROUP BY studentID)));


--2_c. Find the average age of the students who borrowed books whose genre is "Mystery".
SELECT AVG(age) 
FROM (SELECT age 
    FROM Student 
    NATURAL JOIN (SELECT studentID 
                FROM Student 
                INTERSECT (SELECT studentID 
                        FROM Borrows 
                        NATURAL JOIN (SELECT bookID 
                                    FROM Book 
                                    WHERE genre = 'Mystery'))
                ORDER BY studentID));