goalState = [
    (1, 1, 1),
    (2, 1, 2),
    (3, 1, 3),
    (4, 2, 3),
    (5, 3, 3),
    (6, 3, 2),
    (7, 3, 1),
    (8, 2, 1)
]

currentState = [
    (1, 1, 2),
    (2, 1, 3),
    (3, 2, 1),
    (4, 2, 3),
    (5, 3, 3),
    (6, 2, 2),
    (7, 3, 2),
    (8, 1, 1)
]

i = 0
mDistance = 0

while i <= 7:
    if (goalState[i][1] != currentState[i][1]) or (goalState[i][2] != currentState[i][2]):
        mDistance = mDistance + ((abs(goalState[i][1] - currentState[i][1]) + abs(goalState[i][2] - currentState[i][2])))
    i = i + 1

print("Manhattan distance is: ", mDistance)
