graph = {"A": {"B": 5, "C": 3},
        "B": {"D": 2},
        "C": {"D": 1},
        "D": {"A": 8}}

def findPath(graph, st, des, visited, pathLength):
    if st == des:
        return pathLength

    visited.add(st)

    for i in graph[st]:
        if i not in visited:
            length = findPath(graph, i, des, visited, pathLength + graph[st][i])
            print(length)

    visited.remove(st)

    return length

st = input("Enter starting node: ")
des = input("Enter destination node: ")
visited = set()
pathLength = 0

findPath(graph, st, des, visited, pathLength)
