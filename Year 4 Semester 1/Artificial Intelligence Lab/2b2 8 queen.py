queenPlaced = [
    (1, 6, 1),
    (2, 1, 2),
    (3, 5, 3),
    (4, 7, 4),
    (5, 4, 5),
    (6, 3, 6),
    (7, 8, 7),
    (8, 1, 8),
]

pair = 0
i = 0
for i in range(8):
    queen = queenPlaced[i][0]
    queenRowPosition = queenPlaced[i][1]
    queenColumnPosition = queenPlaced[i][2]
    for j in range(8):
        if (queen == queenPlaced[j][0]):
            continue
        elif (queenRowPosition == queenPlaced[j][1]):
            pair = pair + 1
        elif (queenColumnPosition == queenPlaced[j][2]):
            pair = pair + 1
        elif (abs(queenRowPosition-queenPlaced[j][1]) == abs(queenColumnPosition-queenPlaced[j][2])):
            pair = pair + 1
print("Heuristic value is ", pair/2)
