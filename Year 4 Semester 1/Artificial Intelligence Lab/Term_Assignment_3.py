import pandas as pd
import numpy
from sklearn import metrics
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import AdaBoostClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, confusion_matrix
from sklearn.metrics import classification_report
import matplotlib.pyplot as plt
import seaborn as sns
import warnings
warnings.filterwarnings("ignore")

data = pd.read_csv('RedWineQuality.csv')

X = data.iloc[:, :-1]
y = data.iloc[:, 11:]

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 1/3)

standard_scaler = StandardScaler()
X_train = standard_scaler.fit_transform(X_train)
X_test = standard_scaler.transform(X_test)

#model
print("Approach: Logistic regression")
model_Logistic = LogisticRegression()
model_Logistic.fit(X_train, y_train)
prediction_Logistics = model_Logistic.predict(X_test)
print("Predicted value: ", prediction_Logistics)

#Performance matrics
accuracy = accuracy_score(y_test, prediction_Logistics)
print(f'Logistic Regression accuracy: {accuracy:.2f}')
print('Mean Absolute Error:', metrics.mean_absolute_error(y_test, prediction_Logistics))
print('Mean Squared Error:', metrics.mean_squared_error(y_test, prediction_Logistics))
print('Root Mean Squared Error:', numpy.sqrt(metrics.mean_squared_error(y_test, prediction_Logistics)))

#kFold cross validation
k_scores = cross_val_score(model_Logistic, X, y, cv = 100)
print("Cross validation: ",k_scores.mean())


print("----------------------------------------------------------------------------------------------------")

print("Approach: Adaptive Boosting")
#model
model_Ada = AdaBoostClassifier(n_estimators=100, random_state=42)
model_Ada.fit(X_train, y_train)
predictions_Ada = model_Ada.predict(X_test)
print("Predicted value: ", predictions_Ada)

#Performance matrics
print(classification_report(y_test, predictions_Ada))

#kFold cross validation
k_scores = cross_val_score(model_Ada, X, y, cv = 100)
print("Cross validation: ",k_scores.mean())

#plots
sns.lineplot(data = data, x = 'quality', y = 'alcohol', color = 'red')
plt.show()

sns.swarmplot(data=data, x="quality")
plt.show()