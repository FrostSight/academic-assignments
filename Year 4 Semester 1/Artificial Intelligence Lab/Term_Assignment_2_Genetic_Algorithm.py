import random

def create_population(population_size, chromosome_len):
    population = []
    for i in range(population_size):
        chromosome = [random.randint(0, 1) for j in range(chromosome_len)]
        population.append(chromosome)
    return population

def fitness(chromosome):
    return sum(chromosome)

def selection(population, fitness_func):
    population_fitness = [(chromosome, fitness_func(chromosome)) for chromosome in population]
    population_fitness.sort(key=lambda x: x[1], reverse=True)
    selected = [chromosome for (chromosome, _) in population_fitness[:int(len(population)/2)]]
    return selected

def crossover(parent1, parent2):
    crossover_point = random.randint(0, len(parent1))
    child1 = parent1[:crossover_point] + parent2[crossover_point:]
    child2 = parent2[:crossover_point] + parent1[crossover_point:]
    return (child1, child2)

def mutation(chromosome, mutation_probability):
    for i in range(len(chromosome)):
        if random.uniform(0, 1) < mutation_probability:
            chromosome[i] = 1 - chromosome[i]
    return chromosome

def genetic_algorithm(population_size, chromosome_len, fitness_func, mutation_probability, num_generations):
    population = create_population(population_size, chromosome_len)
    for i in range(num_generations):
        population = selection(population, fitness_func)
        new_population = []
        while len(new_population) < population_size:
            parent1, parent2 = random.sample(population, 2)
            child1, child2 = crossover(parent1, parent2)
            new_population.append(mutation(child1, mutation_probability))
            if len(new_population) < population_size:
                new_population.append(mutation(child2, mutation_probability))
        population = new_population
    return population[0]

if __name__ == "__main__":
    chromosome_len = 10
    population_size = 30
    mutation_probability = 0.05
    num_generations = 30
    best_fit_chromosome = genetic_algorithm(population_size, chromosome_len, fitness, mutation_probability, num_generations)
    print("Best chromosome:", best_fit_chromosome)
    print("Fitness:", fitness(best_fit_chromosome))
