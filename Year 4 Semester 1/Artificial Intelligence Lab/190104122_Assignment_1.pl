parent(abdul, rafi).
parent(abdul, rafia).
parent(abdul, nafi).
parent(abdul, shafi).

male(rafi).
male(nafi).
male(shafi).
female(rafia).

parent(karim, asif).
brother(karim, badol).
sister(karim, halima).

uncle(X,Y) :- parent(Z,X), brother(Z,Y).
aunt(X,Y) :- parent(Z,X), sister(Z,Y).

brother(X,Y) :- parent(Z,X), parent(Z,Y), male(Y),not(X=Y).
sister(X,Y) :- parent(Z,X),  parent(Z,Y),female(Y), not(X=Y).


findbrother :- write('Name: '), read(X), write('Brother Name: '), brother(X, Gc), write(Gc), tab(5), fail.
findbrother.

findsister :- write('Name: '), read(X), write('Sister Name: '), sister(X, Gc), write(Gc), tab(5), fail.
findsister.


finduncle :- write('Name :'), read(X), write('Uncle Name : '), uncle(X, Uc), write(Uc), tab(5), fail.
finduncle.

findaunt :- write('Name :'), read(X), write('Aunt Name : '), aunt(X, Au), write(Au), tab(5), fail.
findaunt.
