knowledgeBase = {
    "A": ["B", "C"],
    "B": ["D", "E"],
    "C": ["F"],
    "D": [],
    "E": ["G"],
    "F": ["H"],
    "G": [],
    "H": []
}

knownFacts = {"A"}
target = {"H"}

def forwardChain(knowledgeBase, knownFacts, target):
    factList = list(knownFacts)
    while len(factList) > 0:
        x = factList.pop(0)
        if x in target:
            return True
        for i in knowledgeBase[x]:
            if i not in knownFacts:
                knownFacts.add(i)
                factList.append(i)
                print(knownFacts)
    return False

if forwardChain(knowledgeBase, knownFacts, target):
    print("Target found")
else:
    print("Target not found")