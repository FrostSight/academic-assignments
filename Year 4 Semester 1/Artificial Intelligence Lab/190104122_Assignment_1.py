family1 = [('parent', 'Abdul', 'Rafi', 'male'),
            ('parent', 'Abdul', 'Rafia', 'female'),
            ('parent', 'Karim', 'Nafi', 'female'),
            ('parent', 'Abdul', 'Shafi', 'male')]


def brotherFinding():
    name = input("Person name: ")
    
    i = 0
    flag = False
    while (i <= 3):
        if (family1[i][0] == 'parent' and family1[i][2] == name):
            for j in range(4):
                if (family1[j][0] == 'parent' and (family1[i][1] == family1[j][1]) and family1[j][3] == 'male' and family1[j][2] != name):
                    print('Brother: ', end=' ')
                    print(family1[j][2], end='\n')
                    flag = True
        i = i+1
    if(flag == False):
        print("Name is not in family")

brotherFinding()

def sisterFinding():
    name = input("Person name: ")
    
    flag = False
    i = 0
    while (i <= 3):
        if (family1[i][0] == 'parent' and family1[i][2] == name):
            for j in range(4):
                if (family1[j][0] == 'parent' and (family1[i][1] == family1[j][1]) and family1[j][3] == 'female' and family1[j][2] != name):
                    print('Sister: ', end=' ')
                    print(family1[j][2], end='\n')
                    flag = True
        i = i+1
    if(flag == False):
        print("Name is not in family")

sisterFinding()

family2 = [('parent', 'Abdul', 'Rafi'), 
            ('brother', 'Abdul', 'Badol'),
            ('sister',  'Abdul', 'Allima'), 
            ('brother', 'Abdul', 'Khalid'),
            ('brother', 'Abdul', 'Karim')]

def uncleFinding():
    name = input("Person name: ")
    flag = False
    i = 0
    while (i <= 4):
        if (family2[i][0] == 'parent' and family2[i][2] == name):
            for j in range(5):
                if (family2[j][0] == 'brother' and family2[i][1] == family2[j][1]):
                    print('Uncle: ', end=' ')
                    print(family2[j][2], end='\n')
                    flag = True
        i = i+1
    if(flag == False):
        print("Name is not in family")

uncleFinding()

def auntFinding():
    name = input("Person name: ")
    flag = False
    i = 0
    while (i <= 4):
        if (family2[i][0] == 'parent' and family2[i][2] == name):
            for j in range(5):
                if (family2[j][0] == 'sister' and family2[i][1] == family2[j][1]):
                    print('Aunt: ', end=' ')
                    print(family2[j][2], end='\n')
                    flag = True
        i = i+1
    if(flag == False):
        print("Name is not in family")

auntFinding()